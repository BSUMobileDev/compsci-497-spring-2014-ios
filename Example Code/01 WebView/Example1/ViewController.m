//
//  ViewController.m
//  Example1
//
//  Created by Michael Ziray on 1/21/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *url = [[NSURL alloc] initWithString: @"http://zstudiolabs.com/"];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL: url];
    [self.webView loadRequest: urlRequest];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
