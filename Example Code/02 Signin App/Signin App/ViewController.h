//
//  ViewController.h
//  Signin App
//
//  Created by Michael Ziray on 1/23/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *password;


@end
