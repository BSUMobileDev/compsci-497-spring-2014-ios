//
//  Student.h
//  Student Model
//
//  Created by Michael Ziray on 1/28/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Student : NSObject

@property(nonatomic, strong)NSString *name;
@property(nonatomic, assign)BOOL isEnrolled;
@property(nonatomic, strong)NSArray *classes;



-(BOOL)isAnHonorStudent;


@end
