//
//  Student.m
//  Student Model
//
//  Created by Michael Ziray on 1/28/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "Student.h"

@implementation Student

-(NSString *)description
{
    NSMutableString *studentDescription = [self.name mutableCopy];
    
    [studentDescription appendString: [NSString stringWithFormat:@"\n%d\n", self.isEnrolled] ];
    
//    [studentDescription appendString: [NSString stringWithFormat: @"%@", self.classes]];
    [studentDescription appendString: [self.classes description] ];
    
    return studentDescription;
}



-(BOOL)isAnHonorStudent
{
    if( [self.classes count] > 2 )
    {
        return YES;
    }

    return NO;
}

@end
