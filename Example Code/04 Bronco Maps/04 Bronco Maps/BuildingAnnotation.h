//
//  BuildingAnnotation.h
//  04 Bronco Maps
//
//  Created by Michael Ziray on 2/6/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MapKit/MapKit.h>

@interface BuildingAnnotation : NSObject <MKAnnotation>

@property(nonatomic, copy)NSString *title;
@property(nonatomic, copy)NSString *subtitle;
@property(nonatomic, assign)CLLocationCoordinate2D coordinate;

@end
