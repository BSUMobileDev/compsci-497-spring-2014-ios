//
//  ViewController.m
//  04 Bronco Maps
//
//  Created by Michael Ziray on 1/30/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "MapViewController.h"

#import "WebServicesController.h"
#import "SBJson.h"

#import "BuildingAnnotation.h"
#import "LocationController.h"

#import <MapKit/MapKit.h>
#import "DetailsViewController.h"


@interface MapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapViewController
- (IBAction)addPin:(id)sender
{
    DetailsViewController *detailsViewController = [[DetailsViewController alloc] initWithNibName:@"DetailsViewController" bundle: nil];
    
    [self presentViewController: detailsViewController animated: YES completion: nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [WebServicesController downloadBuildingsDataWithDelegate: self];
    
    
    // Set map type
//    [self.mapView setMapType: MKMapTypeStandard];
//    [self.mapView setMapType: MKMapTypeSatellite];
    [self.mapView setMapType: MKMapTypeHybrid];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(didUpdateLocation:)
                                                 name: kLocationDidChange
                                               object: nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)didUpdateLocation:(NSNotification *)notification
{
    CLLocation *updatedLocation = (CLLocation *)[notification object];
    
    // Zoom in on location
    // MKCoordinateRegion
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *jsonString = [[NSString alloc] initWithData: request.responseData encoding: NSUTF8StringEncoding];
    
    NSArray *buildingsArray = [jsonString JSONValue];
    
    NSDictionary *firstBuilding = [buildingsArray objectAtIndex: 0];
    
    NSString *buildingName = [firstBuilding valueForKey:@"name"];
    
    NSLog(@"%@", buildingName);
    
    
    // Remove all current annotations from map
    for (id<MKAnnotation> annotation in self.mapView.annotations)
    {
        [self.mapView removeAnnotation:annotation];
    }
    
    
    for (NSDictionary *buildingInfo in buildingsArray)
    {
        NSString *buildingName = (NSString *)[buildingInfo objectForKey: @"name"];
        NSString *buildingDescription = (NSString *)[buildingInfo objectForKey:@"description"];
        
        
        NSDictionary *locationInfo = (NSDictionary *)[buildingInfo objectForKey:@"location"];
        
        NSNumber * latitude = (NSNumber *)[locationInfo objectForKey:@"latitude"];
        NSNumber * longitude = (NSNumber *)[locationInfo objectForKey: @"longitude"];

        CLLocationCoordinate2D buildingLocation;
        buildingLocation.latitude  = latitude.doubleValue;
        buildingLocation.longitude = longitude.doubleValue;
        
        // Construct annotation
        BuildingAnnotation *buildingAnnotation = [[BuildingAnnotation alloc] init];
        buildingAnnotation.title = buildingName;
        buildingAnnotation.subtitle = buildingDescription;
        buildingAnnotation.coordinate = buildingLocation;
        
        // Place annotation on map
        [_mapView addAnnotation: buildingAnnotation];
	}
}


@end
