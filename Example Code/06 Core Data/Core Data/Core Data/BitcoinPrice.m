//
//  BitcoinPrice.m
//  Core Data
//
//  Created by Michael Ziray on 2/13/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "BitcoinPrice.h"


@implementation BitcoinPrice

@dynamic lastPrice;
@dynamic timestamp;

@end
