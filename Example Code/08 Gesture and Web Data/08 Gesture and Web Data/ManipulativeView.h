//
//  ManipulativeView.h
//  BSU Football
//
//  Created by Michael Ziray on 7/31/12.
//  Copyright (c) 2012 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManipulativeView : UIView <UIGestureRecognizerDelegate>

@end
