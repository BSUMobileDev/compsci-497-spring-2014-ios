//
//  AppDelegate.h
//  09 Saving Data
//
//  Created by Michael Ziray on 3/18/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
- (NSString *) documentDirectory;
@end
