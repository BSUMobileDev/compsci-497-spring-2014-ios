//
//  AppDelegate.m
//  10 Local Notification
//
//  Created by Michael Ziray on 4/3/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.alertBody = @"This is a local message";
    localNotification.fireDate  = [NSDate dateWithTimeIntervalSinceNow: 6];
    
    [[UIApplication sharedApplication] scheduleLocalNotification: localNotification];
    
    return YES;
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"Got the notification");
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSNumber *launchedNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"launchedNumber"];
    if( launchedNumber == nil )
    {
        launchedNumber = @1;
    }
    else{
        launchedNumber = launchedNumber + 1;
    }
    
    if( launchedNumber > 3 )
    {
        UIAlertView *ratingAlert = [[UIAlertView alloc] initWithTitle: @"Rate Me"
                                                              message: @"Please rate my app"
                                                             delegate: self
                                                    cancelButtonTitle: @"No thanks"
                                                    otherButtonTitles: @"OK, sure", nil];
        [ratingAlert show];
    }
    
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"itms-apps://itunes.apple.com/us/app/netref-wi-fi-router-reference/id472157774?mt=8"]];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
