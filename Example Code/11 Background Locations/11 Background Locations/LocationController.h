//
//  LocationController.h
//
//  Created by Michael Ziray on 2/6/12.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>



#define kLocationDidChange @"kLocationDidChange"


@protocol LocationDelegate <NSObject>

-(void)didUpdateLocation:(CLLocation *)newLocation;

@end


@interface LocationController : NSObject <CLLocationManagerDelegate>
{
    CLLocationManager *currentLocationManager;
@private
    BOOL isReporting;
}

@property (nonatomic, assign) BOOL isReporting;
@property (nonatomic, retain) CLLocationManager *currentLocationManager;


+(LocationController *)sharedLocationController;
+(CLLocation *)currentLocation;

+(void)startLocationReporting;
+(void)stopLocationReporting;

@end
