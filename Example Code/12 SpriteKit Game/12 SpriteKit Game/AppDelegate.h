//
//  AppDelegate.h
//  12 SpriteKit Game
//
//  Created by Michael Ziray on 4/8/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
