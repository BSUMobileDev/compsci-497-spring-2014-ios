//
//  MyScene.m
//  12 SpriteKit Game
//
//  Created by Michael Ziray on 4/8/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "MyScene.h"


static const uint32_t SHIP_CATEGORY    = 0x1; // 1
static const uint32_t BARRIER_CATEGORY = 0x1 << 1; // 2
static const uint32_t EDGE_CATEGORY    = 0x1 << 2; // 4


@interface MyScene()
@property(nonatomic) SKSpriteNode *spaceship;
@property(nonatomic) SKSpriteNode *barrier;

@property(nonatomic) SKAction *tinkSound;
@property(nonatomic) SKEmitterNode *jetFlames;
@end


@implementation MyScene

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
        [self setPhysicsBody: [SKPhysicsBody bodyWithEdgeLoopFromRect: self.frame]];
        self.physicsBody.categoryBitMask = EDGE_CATEGORY;
        [self.physicsWorld setGravity: CGVectorMake(10, 0)];
        [self createShip];
        
        self.barrier = [SKSpriteNode spriteNodeWithColor:[SKColor whiteColor]
                                                             size: CGSizeMake(50, 100)];
        [self.physicsWorld setContactDelegate: self];
        
//        _barrier.physicsBody.contactTestBitMask = BARRIER_CATEGORY;
        _barrier.physicsBody.categoryBitMask = BARRIER_CATEGORY;
        _barrier.position = CGPointMake(size.width/2, 50);
        [_barrier setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: _barrier.frame.size]];
        [_barrier.physicsBody setAffectedByGravity: NO];
        [self addChild: _barrier];
        
        self.tinkSound = [SKAction playSoundFileNamed:@"Tink.caf" waitForCompletion: NO];
        
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
//    for( UITouch *touch in touches )
//    {
//        
//    }
    
    [self.spaceship.physicsBody setVelocity: CGVectorMake(0, 300.0f)];
    
    self.jetFlames.particleBirthRate = 455;
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.jetFlames.particleBirthRate = 0;
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    NSLog(@"contact.bodyA %@", NSStringFromClass( [contact.bodyA class] ));
    NSLog(@"contact.bodyB %@", NSStringFromClass( [contact.bodyB class] ));
    
    if( contact.bodyA.categoryBitMask != EDGE_CATEGORY  && contact.bodyB.categoryBitMask != EDGE_CATEGORY)
    {
        NSLog(@"fired!");
        [_barrier removeFromParent];
    }
    else{
        
    }
    
        
}


-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

-(void)createShip
{
    CGPoint location = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    
    self.spaceship = [SKSpriteNode spriteNodeWithImageNamed:@"Spaceship"];
    [_spaceship setScale: 0.3f];
    [_spaceship setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: _spaceship.frame.size.width/2.5]];
    _spaceship.position = location;
    _spaceship.physicsBody.categoryBitMask    = SHIP_CATEGORY;
    
    _spaceship.physicsBody.contactTestBitMask = EDGE_CATEGORY;

    
    self.jetFlames =
    [NSKeyedUnarchiver unarchiveObjectWithFile:
     [[NSBundle mainBundle] pathForResource:@"Fire" ofType:@"sks"]];
    
    _jetFlames.position = CGPointMake(0,0-_spaceship.frame.size.height*2);
    _jetFlames.particleBirthRate = 0;
    [_spaceship addChild: _jetFlames];

    
    [self addChild:_spaceship];
}


@end
