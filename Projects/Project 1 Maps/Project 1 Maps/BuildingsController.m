//
//  BuildingsController.m
//  Project 1 Maps
//
//  Created by Michael Ziray on 2/13/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "BuildingsController.h"
#import "WebServicesController.h"
#import "BuildingsFactory.h"



static BuildingsController *sharedBuildingsController = nil;




@interface BuildingsController ()

@property(nonatomic, strong)NSMutableArray *buildingsArray;

@end



@implementation BuildingsController


+(BuildingsController *)sharedBuildingsController
{
    if ( sharedBuildingsController == nil)
    {
        sharedBuildingsController = [[BuildingsController alloc] init];
        sharedBuildingsController.buildingsArray = [[NSMutableArray alloc] init];
    }
    return sharedBuildingsController;
}


+(NSArray *)buildings
{
    return [[BuildingsController sharedBuildingsController] buildingsArray];
}



+(void)addBuildings:(NSArray *)newBuildings
{
    [[BuildingsController sharedBuildingsController].buildingsArray addObjectsFromArray: newBuildings];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newBuildings" object: [BuildingsController sharedBuildingsController].buildingsArray];
    
}


+(void)addBuilding:(BuildingAnnotation *)newBuildingAnnotation
{
    [[BuildingsController sharedBuildingsController].buildingsArray addObject: newBuildingAnnotation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newBuildings" object: [BuildingsController sharedBuildingsController].buildingsArray];
}

+(void)refreshBuildings
{
    [WebServicesController downloadBuildingsDataWithDelegate: [BuildingsController sharedBuildingsController]];
}






-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *jsonString = [[NSString alloc] initWithData: request.responseData encoding: NSUTF8StringEncoding];
    
    NSArray *buildingsArray = [NSJSONSerialization JSONObjectWithData: request.responseData
                                                              options: NSJSONReadingAllowFragments
                                                                error: nil];
    
    NSDictionary *firstBuilding = [buildingsArray objectAtIndex: 0];
    
    NSString *buildingName = [firstBuilding valueForKey:@"name"];
    
    NSLog(@"%@", buildingName);
    
    
    
    [BuildingsFactory buildingsFromJSON: request.responseData];
}


@end
