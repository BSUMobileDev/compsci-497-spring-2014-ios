//
//  DetailsViewController.m
//  Project 1 Maps
//
//  Created by Michael Ziray on 2/18/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "DetailsViewController.h"
#import "BuildingsController.h"
#import "BuildingAnnotation.h"


@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UITextField *title;

@end


@implementation DetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)save:(id)sender
{
    BuildingAnnotation *newBuilding = [[BuildingAnnotation alloc] init];
    newBuilding.title = self.title.text;
    [BuildingsController addBuilding: newBuilding];
}

@end
