//
//  LocationController.m
//
//  Created by Michael Ziray on 2/6/12.
//

#import "LocationController.h"


static LocationController *sharedLocationController = nil;

@implementation LocationController


@synthesize isReporting,currentLocationManager;


static CLLocation *currentLocation;


+(CLLocation *)currentLocation
{
	return currentLocation;
}

+(LocationController *)sharedLocationController
{
    if( sharedLocationController == nil )
    {
        sharedLocationController = [[LocationController alloc] init];
        sharedLocationController.isReporting = NO;
    }
    
    return sharedLocationController;
}



+(void)startLocationReporting
{
	[[LocationController sharedLocationController] setCurrentLocationManager: [[CLLocationManager alloc] init]];
    
    // Accuracy
	[sharedLocationController.currentLocationManager setDesiredAccuracy: kCLLocationAccuracyBest];
    
    // Don't update until device has moved at least 10 meters -- saves battery
	[sharedLocationController.currentLocationManager setDistanceFilter: 10];
    
    
	[sharedLocationController.currentLocationManager setDelegate: sharedLocationController];
	[sharedLocationController.currentLocationManager startUpdatingLocation];
}


+(void)stopLocationReporting
{
    [[LocationController sharedLocationController] setIsReporting: NO];
    [sharedLocationController.currentLocationManager stopUpdatingLocation];
    sharedLocationController.currentLocationManager = nil;
}



- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
	if( currentLocation == nil )
	{
		currentLocation = newLocation;
	}
    
    isReporting = YES;
	
	// If it's a relatively recent event, turn off updates to save power
    //NSDate* eventDate = newLocation.timestamp;
    //NSTimeInterval lastUpdateInterval = [eventDate timeIntervalSinceNow];
    //NSLog(@"%+.1f", currentLocation.horizontalAccuracy);
    
    
    
    BOOL isInBackground = NO;
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
        isInBackground = YES;
    }
    
    // Handle location updates as normal, code omitted for brevity.
    // The omitted code should determine whether to reject the location update for being too
    // old, too close to the previous one, too inaccurate and so forth according to your own
    // application design.
    
    if (isInBackground)
    {
        [self sendBackgroundLocationToServer:newLocation];
    }
    else
    {
		if( currentLocation != nil )
		{
			currentLocation = nil;
		}
		currentLocation = newLocation;
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName: kLocationDidChange object: currentLocation];
    }
	
}

-(void) sendBackgroundLocationToServer:(CLLocation *)location
{
    // REMEMBER. We are running in the background if this is being executed.
    // We can't assume normal network access.
    // bgTask is defined as an instance variable of type UIBackgroundTaskIdentifier
    
    // Note that the expiration handler block simply ends the task. It is important that we always
    // end tasks that we have started.
    
//    __block void (^backgroundTaskingBlock)();
//    UIBackgroundTaskIdentifier backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler: 
//                                                 ^{
//                                                     [[UIApplication sharedApplication] endBackgroundTask: backgroundTask];
//                                                 }];
//    
//
//    // AFTER ALL THE UPDATES, close the task
//    if (backgroundTask != UIBackgroundTaskInvalid)
//    {
//        [[UIApplication sharedApplication] endBackgroundTask: backgroundTask];
//        backgroundTask = UIBackgroundTaskInvalid;
//    }
}






@end
