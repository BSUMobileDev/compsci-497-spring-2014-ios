//
//  ViewController.m
//  Project 1 Maps
//
//  Created by Michael Ziray on 2/4/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "ViewController.h"

#import "LocationController.h"
#import "WebServicesController.h"

#import "BuildingsFactory.h"

#import "BuildingAnnotation.h"
#import "BuildingsController.h"
#import "DetailsViewController.h"

#import <MapKit/MapKit.h>


@interface ViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [LocationController startLocationReporting];

    [BuildingsController refreshBuildings];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(updatePins:) name:@"newBuildings" object: nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [WebServicesController downloadBuildingsDataWithDelegate: self];
}



- (IBAction)addPin:(id)sender
{
//    CLLocation *currentLocation = [LocationController currentLocation];
//    
//    CLLocationCoordinate2D coordinates = currentLocation.coordinate;
    
//    [BuildingsController refreshBuildings];
    DetailsViewController *detailsViewController = [[DetailsViewController alloc] initWithNibName:@"DetailsViewController"
                                                                                           bundle: nil];
    [self presentViewController: detailsViewController animated: YES completion: nil];
}



-(void)updatePins:(NSNotification *)notification
{
    // Remove all current annotations from map
    for (id<MKAnnotation> annotation in self.mapView.annotations)
    {
        [self.mapView removeAnnotation:annotation];
    }

    NSArray *mapAnnotations = (NSArray *)[notification object];
    [self.mapView addAnnotations: mapAnnotations];
}


@end
