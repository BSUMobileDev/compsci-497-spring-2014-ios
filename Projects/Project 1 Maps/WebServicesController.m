//
//  WebServicesController.m
//  04 Bronco Maps
//
//  Created by Michael Ziray on 1/30/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "WebServicesController.h"


static WebServicesController *sharedWebServicesController = nil;

#define WEBSERVICES_HOST @"http://zstudiolabs.com/"

#define BUILDINGS_JSON @"labs/compsci497/buildings.json"

#define WS_REQUEST_TIMEOUT 60



@implementation WebServicesController


+(WebServicesController *)sharedWebServicesController
{
    if( sharedWebServicesController == nil )
    {
        sharedWebServicesController = [[WebServicesController alloc] init];
    }
    return sharedWebServicesController;
}


+(void)downloadBuildingsDataWithDelegate:(id<ASIHTTPRequestDelegate>)delegate
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", WEBSERVICES_HOST, BUILDINGS_JSON];
    NSURL *url = [NSURL URLWithString: urlString];
    
    ASIHTTPRequest *getRequest;
    getRequest = [ASIHTTPRequest requestWithURL:url];
    
    [getRequest setRequestMethod: @"GET"];
    
    [getRequest setTimeOutSeconds: WS_REQUEST_TIMEOUT];
    
    [getRequest setCachePolicy: ASIDoNotReadFromCacheCachePolicy];
    
    if( delegate == nil )
    {
        [getRequest setDelegate:self];
    }
    else
    {
        [getRequest setDelegate: delegate];
    }
    
    [getRequest startAsynchronous];
}


@end
